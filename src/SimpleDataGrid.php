<?php

/**
 * SimpleDataGrid Class
 *
 * @package		simpledatagrid
 * @author		Mike Frank <mike.frank@teamsystems.ca>
 */
class SimpleDataGrid
{
    const CUSCTRL_TEXT = 1;
    const CUSCTRL_IMAGE = 2;
    const STDCTRL_EDIT = 3;
    const STDCTRL_DELETE = 4;
    const CUSCTRL_ICON = 5;
    const TYPE_DATE = 1;
    const TYPE_IMAGE = 2;
    const TYPE_ONCLICK = 3;
    const TYPE_ARRAY = 4;
    const TYPE_DOLLAR = 5;
    const TYPE_HREF = 6;
    const TYPE_CHECK = 7;
    const TYPE_PERCENT = 8;
    const TYPE_CUSTOM = 9;
    const TYPE_FUNCTION = 10;
    const ORDER_DESC = 'DESC';
    const ORDER_ASC = 'ASC';
    const TXT_RESET = 'Reset Table';
    const TXT_NORESULTS = 'No results found';

    protected $iResultsPerPage = 10;
    protected $iColumnCount = 0; // Num of columns
    protected $iRowCount = 0; // Number of rows
    protected $bHideHeader = FALSE; // Header visibility
    protected $bHideFooter = FALSE; // Footer visibility
    protected $bHideOrder = FALSE; // Show ordering option
    protected $bAllowFilters = FALSE; // Allow filters or not
    protected $bRowSelect = FALSE; // Enable row selection
    protected $bCreateButton = FALSE; // Show create button
    protected $bResetButton = FALSE; // Show reset grid button
    protected $bPrintButton = FALSE; // Show print button
    protected $bShowRowNumber = FALSE; // Show row numbers
    protected $bHidePageList = FALSE; // Hide page list
    protected $sShowCheckboxes = '';
    protected $sCheckAllCallback = '';
    protected $bAllowResultSel = FALSE;
    protected $aResultsOptions = array ('5' => '5', '10' => '10', '25' => '25', '50' => '50', '100' => '100', '-1' => 'All');
    protected $bPersist = FALSE; // Persist settings
    protected $sSessionName = FALSE;
    protected $iPageNum = 1; // Current page
    protected $sPrefix = ''; // Datagrid prefix
    protected $sPrimary = ''; // Tables primary key column
    protected $sQuery; // SQL query
    protected $aHiddenColumns = array (); // Hidden columns
    protected $aHeaderTitles = array (); // Header titles
    protected $aColumnTypes = array (); // Column types
    protected $aControls = array (); // Row controls, std or custom
    protected $aColumnClasses = array (); // Column classes
    protected $aRowClass = array (); // Row classes
    protected $aDefaultOrder = FALSE; // Default order
    protected $aOrder = FALSE; // Current order
    protected $sOrder = ''; // Raw order value
    protected $aFilter = FALSE; // Current filter
    protected $sFilter = ''; // Raw filter value
    protected $aLimit = FALSE; // Current limit
    protected $oResults; //
    protected $sSelectFields = ''; // Field used to select
    protected $sSelectWhere = ''; // Where clause
    protected $sSelectTable = ''; // Table to read
    protected $sQueryCallback;
    protected $sImagePath = 'https://static.teamsystems.ca/images/datagrid/'; // Path to images
    protected $bUseMaterialIcons = false;

    // Filename of required images
    protected $sImgEdit = 'btn_edit.gif';
    protected $sImgDelete = 'btn_delete.gif';
    protected $sImgCreate = 'btn_create.gif';
    protected $sImgReset = 'btn_reset.gif';
    protected $sImgPrint = 'btn_print.gif';
    protected $sImgResults = 'btn_results.gif';

    public function __construct ()
    {
        $iPage = (post ('page'))
            ? (int) post ('page')
            : 0; // Page number

        $sOrder = (post ('order'))
            ? post ('order')
            : ''; // Order clause

        $sFilter = (post ('filter'))
            ? post ('filter')
            : ''; // Filter clause

        $iResultsPerPage = (post ('resultsperpage'))
            ? (int) post ('resultsperpage')
            : ''; // Results per page clause

        // Prefix
        if ( post ('prefix') )
            $this->setPrefix (post ('prefix'));

        // Handle persistant settings
        if ( $this->getSetting ('persist') === TRUE )
        {
            if ( post ('action') == '' )
            {
                $iPage = $this->getSetting ('page');
                $sFilter = $this->getSetting ('filter');
                $sOrder = $this->getSetting ('order');
                $iResultsPerPage = $this->getSetting ('resultsperpage');
            }
        }
        else
        {
            $this->delSetting ('persist');
            $this->delSetting ('page');
            $this->delSetting ('filter');
            $this->delSetting ('order');
            $this->delSetting ('resultsperpage');
        }

        // Set the results per page
        if ( $iResultsPerPage )
            $this->iResultsPerPage = $iResultsPerPage;
        else
            $this->iResultsPerPage = 10;

        // Set the limit
        if ( empty ($iPage) or $iPage <= 0 )
            $this->_setLimit (0, $this->iResultsPerPage);
        else
            $this->iPageNum = $iPage;

        // Set the order
        if ( $sOrder )
        {
            $this->sOrder = $sOrder;

            list ($sColumn, $sOrder) = $this->_parseInputCond ($sOrder);
            $this->_setOrder ($sColumn, $sOrder);
        }

        // Set the filter
        if ( $sFilter )
        {
            $this->sFilter = $sFilter;

            list ($sColumn, $sValue) = $this->_parseInputCond ($sFilter);
            $this->_setFilter ($sColumn, $sValue);
        }
    }

    /**
     * Hides page drop down selection and replaces it with text
     *
     * @param $bHide Show or hide the page drop down
     */
    public function hidePageSelectList ($bHide = TRUE)
    {
        $this->bHidePageList = $bHide;
    }

    /**
     * Allow filters
     *
     * @param boolean $bAllow
     */
    public function allowFilters ($bAllow = TRUE)
    {
        $this->bAllowFilters = $bAllow;
    }

    /**
     * Hide order functionality
     *
     * @param boolean $bHide
     */
    public function hideOrder ($bHide = TRUE)
    {
        $this->bHideOrder = $bHide;
    }

    /**
     * Hide header row
     *
     * @param boolean $bHide
     */
    public function hideHeader ($bHide = TRUE)
    {
        $this->bHideHeader = $bHide;
    }

    /**
     * Hide footer row
     *
     * @param boolean $bHide
     */
    public function hideFooter ($bHide = TRUE)
    {
        $this->bHideFooter = $bHide;
    }

    /**
     * Show reset control
     *
     * @param string $text Display caption
     */
    public function showReset ($sText = self::TXT_RESET)
    {
        $this->bResetButton = $sText;
    }

    /**
     * Show print control
     *
     * @param string $text Display caption
     */
    public function showPrint ($bShow = TRUE)
    {
        $this->bPrintButton = $bShow;
    }

    /**
     * Show row numbers
     *
     * @param boolean $bShow
     */
    public function showRowNumber ($bShow = TRUE)
    {
        $this->bShowRowNumber = $bShow;
    }

    public function showCheckboxes ($sColumn, $sCheckAllCallback)
    {
        $this->sShowCheckboxes = $sColumn;
        $this->sCheckAllCallback = $sCheckAllCallback;
    }

    public function setImagePath ($sPath)
    {
        $this->sImagePath = $sPath;
    }

    public function setColumnClass ($sColumn, $sClass)
    {
        $this->aColumnClasses[$sColumn] = $sClass;
    }

    public function setRowClass ($sColumn, $sValue, $sClass)
    {
        $this->aRowClass[] = array ('Column' => $sColumn,
            'Value' => $sValue,
            'Class' => $sClass);
    }

    public function allowResultsSelection ($bAllow = TRUE)
    {
        $this->bAllowResultSel = $bAllow;
    }



    /**
     * Set the SELECT query
     *
     * @param string $sFields Feilds to fetch from table. * for all columns
     * @param string $sTable Table to select from
     * @param string $sPrimary Optional primary key column
     * @param string $sWhere Optional where condition
     */
    public function setQuery ($sFields, $sTable, $sPrimary = '', $sWhere = '')
    {
        $this->sPrimary = $sPrimary;

        $this->sSelectFields = $sFields;
        $this->sSelectTable = $sTable;
        $this->sSelectWhere = $sWhere;
    }

    /**
     * Set filter
     *
     * @param string $sColumn Column to apply filter clause on
     * @param string $sValue Value to compare to
     */
    protected function _setFilter ($sColumn, $sValue)
    {
        $this->aFilter = array ('Column' => $sColumn,
            'Value' => $sValue);
    }

    /**
     * Set order
     *
     * @param string $sColumn Column to apply order clause on
     * @param string $iOrder Direction, use ORDER_* const
     */
    protected function _setOrder ($sColumn, $iOrder = self::ORDER_DESC)
    {
        $iOrder = ( $iOrder == self::ORDER_DESC )
            ? self::ORDER_DESC
            : self::ORDER_ASC;

        $this->aOrder = array ('Column' => $sColumn,
            'Order' => $iOrder);
    }

    /**
     * Set the default order when a column was not order by the user
     *
     * @param string $sColumn Column to apply order clause on
     * @param string $iOrder Direction, use ORDER_* const
     */
    public function setDefaultOrder ($sColumn, $iOrder = self::ORDER_DESC)
    {
        $iOrder = ( $iOrder == self::ORDER_DESC )
            ? self::ORDER_DESC
            : self::ORDER_ASC;

        $this->aDefaultOrder = array ('Column' => $sColumn,
            'Order' => $iOrder);
    }

    /**
     * Hides a column
     *
     * @param mixed $sColumn The column to be hidden
     */
    public function hideColumn ($mColumn)
    {
        if ( is_array ($mColumn) )
        {
            $this->aHiddenColumns = array_merge($this->aHiddenColumns, $mColumn);
        }
        else
        {
            $this->aHiddenColumns[] = $mColumn;
        }
    }

    /**
     * Change column header caption
     *
     * @param mixed $mColumn The column name
     * @param string $sHeader The new header caption
     */
    public function setColumnHeader ($mColumn, $sHeader = '')
    {
        if ( is_array ($mColumn) )
        {
            $this->aHeaderTitles = array_merge($this->aHeaderTitles, $mColumn);
        }
        else
        {
            $this->aHeaderTitles[$mColumn] = $sHeader;
        }
    }

    /**
     * Set a column type
     *
     * @param string $sColumn The column to apply the type to
     * @param integer $iType The type of column, use TYPE_* const
     * @param mixed $sCriteria Specific value to each column type
     * @param mixed $sCriteria2 Second specific value to each column type
     */
    public function setColumnType ($sColumn, $iType, $sCriteria = '', $sCriteria2 = '')
    {
        $this->aColumnTypes[$sColumn] = array ($iType, $sCriteria, $sCriteria2);
    }

    /**
     * Sets the maximum amount of rows per page
     *
     * @param integer $iNum Amount of rows per page
     */
    public function setResultsPerPage ($iNum)
    {
        $this->iResultsPerPage = (int) $iNum;
        $this->_setLimit (0, (int) $iNum);
    }

    /**
     * Set datagrid prefix
     *
     * @param string $sPrefix Prefix string
     */
    public function setPrefix ($sPrefix)
    {
        $this->sPrefix = $sPrefix;
    }

    protected function setSetting ($sName, $mValue)
    {
        SM()->session->set ('dg' . $this->sPrefix . '.' . $sName . '.' . post ('response'), $mValue);
    }

    protected function getSetting ($sName)
    {
        return SM()->session->get ('dg' . $this->sPrefix . '.' . $sName . '.' . post ('response'));
    }

    protected function delSetting ($sName)
    {
        SM()->session->delete ('dg' . $this->sPrefix . '.' . $sName . '.' . post ('response'));
    }

    /**
     * Persist settings over the visitors session
     *
     * @param bool $bPersist
     */
    public function persistSettings ($bPersist = TRUE)
    {
        $this->bPersist = $bPersist;

        if ( $this->bPersist )
        {
            $this->setSetting ('persist', TRUE);
            $this->setSetting ('page', $this->iPageNum);
            $this->setSetting ('filter', $this->sFilter);
            $this->setSetting ('order', $this->sOrder);
            $this->setSetting ('resultsperpage', $this->iResultsPerPage);
        }
    }

    /**
     * Adds a standard control to a row
     *
     * @param integer $iType The type of standard control, use STDCTRL_* const
     * @param string $sAction The action of the control (onclick code or href link)
     * @param integer $iActionType The type of action, use TYPE_ONCLICK or TYPE_HREF
     */
    public function addStandardControl ($iType, $sAction, $iActionType = self::TYPE_ONCLICK)
    {
        $sAction = $this->_parseLinkAction ($sAction, $iActionType);

        switch ( $iType )
        {
            case self::STDCTRL_EDIT:
                $this->aControls[] = '<a ' . $sAction . '><img src="' . $this->sImagePath . $this->sImgEdit . '" alt="Edit" title="Edit" class="tbl-control-image"></a>';
                break;
            case self::STDCTRL_DELETE:
                $this->aControls[] = '<a ' . $sAction . '><img src="' . $this->sImagePath . $this->sImgDelete . '" alt="Delete" title="Delete" class="tbl-control-image"></a>';
                break;
            default:
                // Invalid standard control
                break;
        }
    }

    /**
     * Adds a custom control to a row
     *
     * @param integer $iType The type of custom control, use CUSCTRL_* const
     * @param string $sAction The action of the control (onclick code or href link)
     * @param integer $iActionType The type of action, use TYPE_ONCLICK or TYPE_HREF
     * @param string $sText The textual description of the control
     * @param string $sImagePath The location of the image if type is self::CUSCTRL_IMAGE or material icon if type is self::CUSCTRL_ICON
     */
    public function addCustomControl ($iType = self::CUSCTRL_TEXT, $sAction = '#', $iActionType = self::TYPE_ONCLICK, $sText = '', $sImageSrc = '')
    {
        $sAction = $this->_parseLinkAction ($sAction, $iActionType);

        switch ( $iType )
        {
            case self::CUSCTRL_ICON:
                $this->aControls[] = '<a ' . $sAction . '><span title="' . $sText . '" class="tbl-control-image material-icon">' . $sImageSrc . '</span></a>';
                break;
            case self::CUSCTRL_IMAGE:
                $this->aControls[] = '<a ' . $sAction . '><img src="' . $sImageSrc . '" alt="' . $sText . '" title="' . $sText . '" class="tbl-control-image"></a>';
                break;
            default: // Default to text
                $this->aControls[] = '<a ' . $sAction . '>' . $sText . '</a>';
                break;
        }
    }

    /**
     * Adds a create control above the table
     *
     * @param string $sAction The action associated to the create (onclick code or href link)
     * @param integer $iActionType The type of action, use TYPE_ONCLICK or TYPE_HREF
     * @param string $sText The textual description of the create
     */
    public function showCreateButton ($sAction, $iActionType = self::TYPE_ONCLICK, $sText = 'New Record')
    {
        $sAction = $this->_parseLinkAction ($sAction, $iActionType);

        $this->bCreateButton = array ('Action' => $sAction,
            'Text' => $sText);
    }

    /**
     * Adds ability to select a entire row
     *
     * @param string $onclick The JS function to call when a row is clicked
     */
    public function addRowSelect ($sOnClick)
    {
        $this->bRowSelect = $sOnClick;
    }

    /**
     * Data sanitization and control for filters and ordering
     *
     * @param string $sValue The value to be sanitized and parsed
     */
    protected function _parseInputCond ($sValue)
    {
        return explode (':', preg_replace ("/[\\'\\\"\\<\\>\\\\]/", '%', $sValue), 2);
    }

    public function setQueryCallback ($sQueryCallback)
    {
        $this->sQueryCallback = $sQueryCallback;
    }

    /**
     * If set to true, the simple data grid will use material design icons instead of our custom icons
     * 
     * @param bool $bUseMaterialIcons
     */
    public function setUseMaterialIcons(bool $bUseMaterialIcons = true)
    {
        $this->bUseMaterialIcons = $bUseMaterialIcons;
    }

    /**
     * Replaces our variables place holders with values
     *
     * @param array $aRow The row associated array
     * @param string $sAct The string containing place holders to replace
     * @return string
     */
    protected function _parseVariables ($aRow, $sAct)
    {
        // The only way we get an array for $sAct is for parameters from a column type of function
        if ( is_array ($sAct) )
        {
            // Loop through each passed param and replace variables where necessary
            foreach ($sAct as $sKey => $sValue)
                $sAct[$sKey] = $this->_parseVariables ($aRow, $sValue);

            return $sAct;
        }

        // %_P% is an alias for the primary key, replace it with the primary key
        if ( $this->sPrimary )
            $sAct = str_replace ('%_P%', '%' . $this->sPrimary . '%', $sAct);

        $aVars = array ();
        preg_match_all ("/%(.[^%]*)%/", $sAct, $aVars);

        foreach ( $aVars[0] as $v )
        {
            $sIndex = str_replace ('%', '', $v);

            if ( array_key_exists ($sIndex, $aRow) )
                $sAct = str_replace ($v, isset($aRow[$sIndex]) ? $aRow[$sIndex] : '', $sAct);
        }

        return $sAct;
    }

    /**
     * Builds a link action
     *
     * @param string $sAction The action
     * @param integer $iActionType The type of actions (onclick code or href link)
     * @return string
     */
    protected function _parseLinkAction ($sAction, $iActionType)
    {
        if ( $iActionType == self::TYPE_ONCLICK )
            $sAction = 'href="javascript:;" onclick="' . $sAction . '"';
        else
            $sAction = 'href="' . $sAction . '"';

        return $sAction;
    }

    /**
     * Sets the limit by clause
     *
     * @param integer $iLow The minimum row number
     * @param integer $iHigh The maximum row number
     */
    protected function _setLimit ($iLow, $iHigh)
    {
        $this->aLimit = array ('Low' => $iLow,
            'High' => $iHigh);
    }

    /**
     * Creates the table header
     *
     */
    protected function _buildHeader ()
    {
        // If entire header is hidden, skip all together
        if ( $this->bHideHeader === TRUE )
            return '';

        $sReturn = '<thead><tr>';

        $aHeaders = array ();
        for($i = 0; $i < $this->oResults->columnCount (); $i++)
        {
            $aColumn = $this->oResults->getColumnMeta ($i);
            $aHeaders[] = $aColumn['name'];
        }

        $this->iColumnCount = count ($aHeaders);

        // Add a blank column if the row number is to be shown
        if ( $this->bShowRowNumber === TRUE )
        {
            $this->iColumnCount++;
            $sReturn .= '<td class="tbl-header">&nbsp;</td>';
        }

        // Add a blank column if the checkboxes are on
        if ( ! empty ($this->sShowCheckboxes) )
        {
            $this->iColumnCount++;
            $sReturn .= '<td class="tbl-header tbl-print-hide"><input type="checkbox" name="chk'.$this->sPrefix.'All" id="chk'.$this->sPrefix.'All" onclick="'.$this->sCheckAllCallback.'" /></td>';
        }

        // Loop through each header and output it
        foreach ($aHeaders as $t)
        {
            // Skip column if hidden
            if ( in_array ($t, $this->aHiddenColumns) )
            {
                $this->iColumnCount--;
                continue;
            }

            // Check for header caption overrides
            if ( array_key_exists ($t, $this->aHeaderTitles) )
                $sHeader = $this->aHeaderTitles[$t];
            else
                $sHeader = ucwords ( strtolower ( str_replace ('_', ' ', $t) ) ); // At least try to make the header look nice

            if ( $this->bHideOrder === TRUE )
            {
                $sReturn .= '<td class="tbl-header">' . $sHeader; // Prevent the user from changing order
            }
            else
            {
                $iOrder = self::ORDER_ASC;
                if ( $this->aOrder and $this->aOrder['Column'] == $t )
                {
                    $iOrder = ($this->aOrder['Order'] == self::ORDER_ASC)
                        ? self::ORDER_DESC
                        : self::ORDER_ASC;
                }

                $sReturn .= '<td class="tbl-header">';
                if ($this->bUseMaterialIcons) {
                    $sReturn .= '<div style="display: flex; justify-content: center; align-items: center;">';
                }

                $sReturn .= '<a href="javascript:;"';
                if ($this->bUseMaterialIcons) {
                    $sReturn .= ' class="material-icon-center"';
                }
                $sReturn .= ' onclick="dg' . $this->sPrefix . '.setOrder (\'' . $t . '\', \'' . $iOrder . '\');">' . $sHeader . "</a>";

                // Show the user the order image if set
                if ( $this->aOrder and $this->aOrder['Column'] == $t ) {
                    if ($this->bUseMaterialIcons) {
                        $sIcon = match ($this->aOrder['Order']) {
                            self::ORDER_ASC => 'arrow_drop_up',
                            self::ORDER_DESC => 'arrow_drop_down',
                        };
                        $sReturn .= '&nbsp;<span class="material-icon">' . $sIcon . '</span>';
                    } else {
                        $sReturn .= '&nbsp;<img src="' . $this->sImagePath . 'sort_' . strtolower ($this->aOrder['Order']) . '.gif" class="tbl-order">';
                    }
                }
            }

            // Add filters if allowed and only if the column type is not "special"
            if ( $this->bAllowFilters and  ! array_key_exists ($t, $this->aColumnTypes) )
            {
                if ( is_array($this->aFilter) and $this->aFilter['Column'] == $t and ! empty ($this->aFilter['Value']) )
                {
                    $sFilterDisplay = 'block';
                    $sFilterValue = $this->aFilter['Value'];
                }
                else
                {
                    $sFilterDisplay = 'none';
                    $sFilterValue = '';
                }

                if ($this->bUseMaterialIcons) {
                    $sReturn .= '&nbsp;<a href="javascript:;" onclick="dg' . $this->sPrefix . '.showHideFilter (\'' . $t . '\');"><span class="material-icon tbl-filter-image tbl-print-hide">search</span></a><br><div class="tbl-filter-box" id="' . $this->sPrefix . 'filter-' . $t . '" style="display:' . $sFilterDisplay . '"><input type="text" size="6" id="' . $this->sPrefix . 'filter-value-' . $t . '" value="' . $sFilterValue . '" onkeypress="dg' . $this->sPrefix . '.setFilterOnKeyPress(\'' . $t . '\')">&nbsp;<a href="javascript:;" onclick="dg' . $this->sPrefix . '.setFilter(\'' . $t . '\')">filter</a></div>';
                } else {
                    $sReturn .= '<a href="javascript:;" onclick="dg' . $this->sPrefix . '.showHideFilter (\'' . $t . '\');"><img src="' . $this->sImagePath . 'filter.gif" class="tbl-filter-image tbl-print-hide"></a><br><div class="tbl-filter-box" id="' . $this->sPrefix . 'filter-' . $t . '" style="display:' . $sFilterDisplay . '"><input type="text" size="6" id="' . $this->sPrefix . 'filter-value-' . $t . '" value="' . $sFilterValue . '" onkeypress="dg' . $this->sPrefix . '.setFilterOnKeyPress(\'' . $t . '\')">&nbsp;<a href="javascript:;" onclick="dg' . $this->sPrefix . '.setFilter(\'' . $t . '\')">filter</a></div>';
                }
            }

            if ($this->bUseMaterialIcons) {
                $sReturn .= '</div>';
            }
            $sReturn .= '</td>';
        }

        // If we have controls, add a blank column
        if ( count ($this->aControls) > 0 )
        {
            $this->iColumnCount++;
            $sReturn .= '<td class="tbl-header">&nbsp;</td>';
        }

        $sReturn .= '</tr></thead>';

        return $sReturn;
    }

    /**
     * Creates the table footer
     *
     * @param integer $iFirst The row number of the first row
     * @param integer $iLast The row number of the last row
     */
    protected function _buildFooter ($iFirst = 0, $iLast = 0)
    {
        // Skip adding the footer if it is hidden
        if ( $this->bHideFooter === TRUE )
            return '';

        $iPages = ceil ($this->iRowCount / $this->iResultsPerPage); // Total number of pages

        $sReturn = '<tfoot><tr class="tbl-footer"><td class="tbl-nav" colspan="' . $this->iColumnCount . '"><table width="100%" class="tbl-footer"><tr><td width="33%" class="tbl-found">Found <em>' . $this->iRowCount . '</em> result' . (($this->iRowCount == 1) ? '' : 's');

        if ( $this->iRowCount > 0 )
            $sReturn .= ', showing <em>' . $iFirst . '</em> to <em>' . $iLast . '</em>';

        $sReturn .= '</td><td wdith="33%" class="tbl-pages">';

        if ( $iPages > 0 )
        {
            if ($this->bUseMaterialIcons) {
                $sReturn .= '<div style="display: flex; justify-content: center; align-items: center;">';
            }

            // Handle results that span multiple pages
            if ( $this->iRowCount > $this->iResultsPerPage )
            {
                if ($this->bUseMaterialIcons) {
                    if ($this->iPageNum > 1) {
                        $sReturn .= '<a href="javascript:;" onclick="dg' . $this->sPrefix . '.setPage(1)"><span class="material-icon tbl-arrows tbl-print-hide" title="First Page">first_page</span></a><a href="javascript:;" onclick="dg' . $this->sPrefix . '.setPage (' . ($this->iPageNum - 1) . ');"><span class="material-icon tbl-arrows tbl-print-hide" title="Previous Page">chevron_left</span></a>';
                    }
                } else {
                    if ( $this->iPageNum > 1 ) {
                        $sReturn .= '<a href="javascript:;" onclick="dg' . $this->sPrefix . '.setPage(1)"><img src="' . $this->sImagePath . 'arrow_first.gif" class="tbl-arrows tbl-print-hide" alt="&lt;&lt;" title="First Page"></a><a href="javascript:;" onclick="dg' . $this->sPrefix . '.setPage (' . ($this->iPageNum - 1) . ');"><img src="' . $this->sImagePath . 'arrow_left.gif" class="tbl-arrows tbl-print-hide" alt="&lt;" title="Previous Page"></a>';
                    } else {
                        $sReturn .= '<img src="' . $this->sImagePath . 'arrow_first_disabled.gif" class="tbl-arrows tbl-print-hide" alt="&lt;&lt;" title="First Page"><img src="' . $this->sImagePath . 'arrow_left_disabled.gif" class="tbl-arrows tbl-print-hide" alt="&lt;" title="Previous Page">';
                    }
                }

                // Special thanks to ionut for the next few lines
                $iStartPage = ( $this->iPageNum > 10 )
                    ? $this->iPageNum - 10
                    : 1;

                $iEndPage = ( ($iPages - 10) > $this->iPageNum )
                    ? $this->iPageNum + 10
                    : $iPages;

                // Only display a portion of the selectable pages
                for ($i = $iStartPage; $i <= $iEndPage; $i++)
                {
                    if ( $i == $this->iPageNum )
                        $sReturn .= '&nbsp;<span class="page-selected">' . $i . '</span>&nbsp;';
                    else
                        $sReturn .= '&nbsp;<a href="javascript:;" onclick="dg' . $this->sPrefix . '.setPage (' . $i . ');">' . $i . '</a>&nbsp;';
                }

                if ($this->bUseMaterialIcons) {
                    if ($this->iPageNum < $iPages - 1) {
                        $sReturn .= '<a href="javascript:;" onclick="dg' . $this->sPrefix . '.setPage (' . ($this->iPageNum + 1) . ');"><span class="material-icon tbl-arrows tbl-print-hide" title="Next Page">chevron_right</span></a><a href="javascript:;" onclick="dg' . $this->sPrefix . '.setPage (' . $iPages . ');"><span class="material-icon tbl-arrows tbl-print-hide" title="Last Page">last_page</span></a>';
                    }
                } else {
                    if ( $this->iPageNum < $iPages - 1 ) {
                        $sReturn .= '<a href="javascript:;" onclick="dg' . $this->sPrefix . '.setPage (' . ($this->iPageNum + 1) . ');"><img src="' . $this->sImagePath . 'arrow_right.gif" class="tbl-arrows tbl-print-hide" alt="&gt;" title="Next Page"></a><a href="javascript:;" onclick="dg' . $this->sPrefix . '.setPage (' . $iPages . ');"><img src="' . $this->sImagePath . 'arrow_last.gif" class="tbl-arrows tbl-print-hide" alt="&gt;&gt;" title="Last Page"></a>';
                    } else {
                        $sReturn .= '<img src="' . $this->sImagePath . 'arrow_right_disabled.gif" class="tbl-arrows tbl-print-hide" alt="&gt;" title="Next Page"><img src="' . $this->sImagePath . 'arrow_last_disabled.gif" class="tbl-arrows tbl-print-hide" alt="&gt;&gt;" title="Last Page">';
                    }
                }
            }

            $sReturn .= '</div></td><td width="33%" class="tbl-page">';

            // Only show page section if we have more than one page

            $sReturn .= 'Page ';
            if ( ! $this->bHidePageList and $iPages > 1 )
            {
                // Create a selectable drop down list for pages
                $sReturn .= '<select name="tbl-page" onchange="dg' . $this->sPrefix . '.setPage (this.options[this.selectedIndex].value);">';

                for ($x = 1; $x <= $iPages; $x++)
                {
                    $sReturn .= '<option value="' . $x . '"';
                    if ( $x == $this->iPageNum )
                        $sReturn .= ' selected="selected"';
                    $sReturn .= '>' . $x . '</option>';
                }

                $sReturn .= '</select>';
            }
            else
                $sReturn .= $this->iPageNum; // Just write the page number, nothing to fancy

            $sReturn .= ' of ' . $iPages;
        }

        $sReturn .= '</td></tr></table></td></tr></tfoot>';

        return $sReturn;
    }

    /**
     * Builds row controls
     *
     * @param array $aRow The row associated array
     */
    protected function _buildControls ($aRow)
    {
        $sReturn = '';

        // Add controls as needed
        if ( count($this->aControls) > 0 )
        {
            $sReturn .= '<td class="tbl-controls tbl-print-hide">';

            foreach ($this->aControls as $sControl)
                $sReturn .= $this->_parseVariables ($aRow, $sControl) . ' ';

            $sReturn .= '</td>';
        }

        return $sReturn;
    }

    /**
     * Outputs the datagrid to the screen
     *
     */
    public function printTable ()
    {
        $aReturn = [];
        $sDataGrid = '';

        // Set the limit
        $this->_setLimit (($this->iPageNum - 1) * $this->iResultsPerPage, $this->iResultsPerPage);


        // ### FILTER #######################################################################
        $aFilterWhereConditions = [];
        $aFilterValues = [];
        if ($this->sSelectWhere && \is_array($this->sSelectWhere)) {
            $aFilterWhereConditions[] = '('.$this->sSelectWhere[0].')';
            $aFilterValues[] = \array_splice($this->sSelectWhere, 1);
        } elseif ($this->sSelectWhere && \is_string($this->sSelectWhere)) {
            $aFilterWhereConditions[] = '('.$this->sSelectWhere.')';
        }

        if ($this->bAllowFilters && $this->aFilter) {
            if (!\strstr($this->aFilter['Value'], '%')) {
                $sFilterValue = '%'.$this->aFilter['Value'].'%';
            } else {
                $sFilterValue = $this->aFilter['Value'];
            }

            $aFilterWhereConditions[] = "(`".$this->aFilter['Column']."` LIKE ?)";
            $aFilterValues[] = $sFilterValue;
        }

        $aFilterQuery = null;
        if (\count($aFilterWhereConditions)) {
            $aFilterQuery[] = \implode(' AND ', $aFilterWhereConditions);
            $aFilterQuery = \array_merge($aFilterQuery, $aFilterValues);
        }

        // ### ORDER #######################################################################
        // If a default order has been set and no order was set by the user, set the order to the default
        if ( $this->aDefaultOrder && !$this->aOrder ) {
            $this->aOrder = [
                'Column' => $this->aDefaultOrder['Column'],
                'Order' => $this->aDefaultOrder['Order'],
            ];
        }

        $sOrder = '';
        if ( $this->aOrder )
            $sOrder = $this->aOrder['Column'] . ' ' . $this->aOrder['Order'];

        // Count the number of rows without the limit clause
        $this->iRowCount = (int) \SM()->db->selectOneValue ('COUNT(*)', $this->sSelectTable, $aFilterQuery);

        // Make sure we don't run into problems with going over our limits
        if ( (($this->iPageNum - 1) * $this->iResultsPerPage) > $this->iRowCount )
        {
            // Shit... we have a problem
            $this->iPageNum = 1;
            $this->_setLimit (0, $this->iResultsPerPage);
        }

        // ### LIMIT #######################################################################
        $sLimit = '';
        if ( $this->aLimit )
        {
            $sLimit = ( $this->aLimit['High'] > 0 )
                ? $this->aLimit['Low'] . ', ' . $this->aLimit['High']
                : '';
        }

        if ( ! empty ($this->sQueryCallback) )
        {
            \call_user_func_array ($this->sQueryCallback, [$this->sSelectFields, $this->sSelectTable, $aFilterQuery, $sOrder, $sLimit]);
        }

        if ( ! empty ($sOrder) )
            $sOrder = 'ORDER BY ' . $sOrder;

        if ( ! empty ($sLimit) )
            $sLimit = 'LIMIT ' . $sLimit;

        $sQuery = \SM()->db->buildQuery('select', [$this->sSelectFields, $this->sSelectTable, $aFilterQuery]);

        try
        {
            $this->oResults = SM()->db->query ($sQuery . ' ' . $sOrder . ' ' . $sLimit);
        }
        catch ( Exception $oEx )
        {
            // Inform the user of any errors. Commonly caused when a column is specified in the filter or order clause that does not exist
            SM()->output->setType ('text/javascript');
            echo 'alert ("Oops! We ran into a problem while trying to output the table.");';
            return;
        }

        // Output the create button
        if ( $this->bCreateButton ) {
            if ($this->bUseMaterialIcons) {
                $sDataGrid .= '<div class="tbl-create tbl-print-hide"><a ' . $this->bCreateButton['Action'] . ' class="material-icon-center" title="' . $this->bCreateButton['Text'] . '"><span class="material-icon tbl-create-image">add_circle</span>' . $this->bCreateButton['Text'] . '</a></div>';
            } else {
                $sDataGrid .= '<span class="tbl-create tbl-print-hide"><a ' . $this->bCreateButton['Action'] . ' title="' . $this->bCreateButton['Text'] . '"><img src="' . $this->sImagePath . $this->sImgCreate . '" class="tbl-create-image">' . $this->bCreateButton['Text'] . '</a></span>';
            }
        }

        // Output the reset button
        if ( $this->bResetButton ) {
            if ($this->bUseMaterialIcons) {
                $sDataGrid .= '<div class="tbl-reset tbl-print-hide"><a href="javascript:;" class="material-icon-center" onclick="dg' . $this->sPrefix . '.reset ();" title="' . $this->bResetButton .'"><span class="material-icon tbl-reset-image">refresh</span>' . $this->bResetButton .'</a></div>';
            } else {
                $sDataGrid .= '<span class="tbl-reset tbl-print-hide"><a href="javascript:;" onclick="dg' . $this->sPrefix . '.reset ();" title="' . $this->bResetButton .'"><img src="' . $this->sImagePath . $this->sImgReset . '" class="tbl-reset-image">' . $this->bResetButton .'</a></span>';
            }
        }

        // Output the print button
        if ( $this->bPrintButton === TRUE ) {
            if ($this->bUseMaterialIcons) {
                $sDataGrid .= '<div class="tbl-print tbl-print-hide"><a href="javascript:;" class="material-icon-center" onclick="dg' . $this->sPrefix . '.print ();" title="Print This Page"><span class="material-icon tbl-print-image">print</span>Print This Page</a></div>';
            } else {
                $sDataGrid .= '<span class="tbl-print tbl-print-hide"><a href="javascript:;" onclick="dg' . $this->sPrefix . '.print ();" title="Print This Page"><img src="' . $this->sImagePath . $this->sImgPrint . '" class="tbl-print-image">Print This Page</a></span>';
            }
        }

        // Output the results per page control
        if ( $this->bAllowResultSel )
        {
            if ($this->bUseMaterialIcons) {
                $sDataGrid .= '<span class="material-icon-center tbl-rpp tbl-print-hide"><span class="material-icon tbl-rpp-image">table</span>Results Per Page: <select name="rpp'.$this->sPrefix.'" id="rpp'.$this->sPrefix.'" onchange="dg' . $this->sPrefix . '.setResultsPerPage (this.value);">';
            } else {
                $sDataGrid .= '<span class="tbl-rpp tbl-print-hide"><img src="' . $this->sImagePath . $this->sImgResults . '" class="tbl-rpp-image">Results Per Page: <select name="rpp'.$this->sPrefix.'" id="rpp'.$this->sPrefix.'" onchange="dg' . $this->sPrefix . '.setResultsPerPage (this.value);">';
            }
            foreach ($this->aResultsOptions as $sValue => $sText)
            {
                $sDataGrid .= '<option value="' . $sValue . '"';
                if ( $sValue == $this->iResultsPerPage )
                    $sDataGrid .= ' selected="selected"';
                $sDataGrid .= '>' . $sText . '</option>';
            }
            $sDataGrid .= '</select></span>';
        }

        $sDataGrid .= '<table class="tbl">';

        $sDataGrid .= $this->_buildHeader ();

        $sDataGrid .= '<tbody>';

        $i = 0;
        $iFirst = 0;
        $iLast = 0;

        if ( $this->iRowCount <= 0 )
        {
            $sDataGrid .= '<tr><td colspan="' . $this->iColumnCount . '" class="tbl-noresults">' . self::TXT_NORESULTS . '</td></tr>';
        }
        else
        {

            //foreach ($aResults as $aRow)
            while ( $aRow = $this->oResults->fetch (PDO::FETCH_ASSOC))
            {
                $aRowClasses = array ('tbl-row');
                $sClassOddEven = 'tbl-row-' . (($i % 2) ? 'odd' : 'even');
                $aRowClasses[] = $sClassOddEven; // Switch up the bgcolors on each row

                // Handle row selects
                $sRowSelect = '';
                if ( $this->bRowSelect )
                {
                    $aRowClasses[] = 'tbl-row-highlight';
                    $sRowSelect = ' onclick="' . $this->_parseVariables ($aRow, $this->bRowSelect) . '"';
                }

                // Handle setting row classess
                foreach ($this->aRowClass as $aRowClass)
                {
                    if ( array_key_exists ($aRowClass['Column'], $aRow) )
                    {
                        if ( $aRow[$aRowClass['Column']] == $aRowClass['Value'] )
                            $aRowClasses[] = $aRowClass['Class'];
                    }
                }

                $sDataGrid .= '<tr' . $sRowSelect . ' class="' . implode (' ', $aRowClasses) . '">'; // Add the TR to the datagrid string

                $iLine = ( $this->iPageNum == 1 )
                    ? $i + 1
                    : $i + 1 + (($this->iPageNum - 1) * $this->iResultsPerPage);

                $iLast = $iLine; // Last line
                if ( $iFirst == 0 )
                    $iFirst = $iLine; // First line

                if ( $this->bShowRowNumber === TRUE )
                    $sDataGrid .= '<td class="tbl-row-num tbl-cell">' . $iLine . '</td>';

                if ( ! empty ($this->sShowCheckboxes) )
                {
                    $sCheckboxName = $this->_parseVariables ($aRow, $this->sShowCheckboxes);
                    $sDataGrid .= '<td class="tbl-checkbox tbl-cell tbl-print-hide"><input type="checkbox" name="chk'.$this->sPrefix.'['.$sCheckboxName.']" id="chk'.$this->sPrefix.'_'.$sCheckboxName.'" value="'.$sCheckboxName.'" /></td>';
                }

                foreach ($aRow as $sKey => $sValue)
                {
                    // Skip if column is hidden
                    if ( in_array ($sKey, $this->aHiddenColumns) )
                        continue;

                    // Apply a column type to the value
                    if ( array_key_exists ($sKey, $this->aColumnTypes) )
                    {
                        list ($iType, $sCriteria, $sCriteria2) = $this->aColumnTypes[$sKey];

                        switch ( $iType )
                        {
                            case self::TYPE_ONCLICK:
                                if ( ! empty ($sValue) )
                                    $sValue = '<a href="javascript:;" onclick="' . $this->_parseVariables ($aRow, $sCriteria) . '">' . $sValue . '</a>';
                                break;

                            case self::TYPE_HREF:
                                if ( ! empty ($sValue) )
                                    $sValue = '<a href="' . $this->_parseVariables ($aRow, $sCriteria) . '">' . $sValue . '</a>';
                                break;

                            case self::TYPE_DATE:
                                if ( empty ($sValue) )
                                    $sValue = '';
                                else
                                {
                                    if ( $sCriteria2 === TRUE )
                                        $sValue = date ($sCriteria, strtotime ($sValue));
                                    else
                                        $sValue = date ($sCriteria, $sValue);
                                }
                                break;

                            case self::TYPE_IMAGE:
                                if ( ! empty ($sValue) )
                                    $sValue = '<img src="' . $this->_parseVariables ($aRow, $sCriteria) . '" id="' . $this->sPrefix . $sKey . '-' . $i . '"' . $sCriteria2 . '>';
                                break;

                            case self::TYPE_ARRAY:
                                if ( array_key_exists ($sValue, $sCriteria) )
                                    $sValue = $sCriteria[$sValue];
                                break;

                            case self::TYPE_CHECK:
                                if ( $sValue == '1' or $sValue == 'yes' or $sValue == 'y' or $sValue == 'true' or ($sCriteria != '' and $sValue == $sCriteria))
                                    if ($this->bUseMaterialIcons) {
                                        $sValue = '<span class="material-icon">check_circle</span>';
                                    } else {
                                        $sValue = '<img src="' . $this->sImagePath . 'check.gif">';
                                    }
                                else
                                    $sValue = '';
                                break;

                            case self::TYPE_PERCENT:
                                if ( $sCriteria === TRUE )
                                    $sValue *= 100; // Value is in decimal format

                                $sValue = round ($sValue); // Round to the nearest decimal

                                $sValue .= '%';

                                // Apply a bar if an array is supplied via sCriteria2
                                if ( is_array ($sCriteria2) )
                                    $sValue = '<div style="background: ' . $sCriteria2['Back'] . '; width: ' . $sValue . '; color: ' . $sCriteria2['Fore'] . ';">' . $sValue . '</div>';
                                break;

                            case self::TYPE_DOLLAR:
                                $sValue = '$' . number_format ($sValue, ( ( $sCriteria === TRUE ) ? 0 : 2) );
                                break;

                            case self::TYPE_CUSTOM:
                                $sValue = $this->_parseVariables ($aRow, $sCriteria);
                                break;

                            case self::TYPE_FUNCTION:
                                if ( is_array ($sCriteria2) )
                                    $sValue = call_user_func_array ($sCriteria, $this->_parseVariables ($aRow, $sCriteria2));
                                else
                                    $sValue = call_user_func ($sCriteria, $this->_parseVariables ($aRow, $sCriteria2));
                                break;

                            default:
                                // Invalid column type
                                break;
                        }
                    }

                    // Apply column classess
                    $aColumnClasses = array ('tbl-cell');
                    if ( array_key_exists ($sKey, $this->aColumnClasses) )
                        $aColumnClasses[] = $this->aColumnClasses[$sKey];

                    $sDataGrid .= '<td class="' . implode (' ', $aColumnClasses) . '">' . $sValue . '</td>';
                }

                $sDataGrid .= $this->_buildControls ($aRow);

                $sDataGrid .= '</tr>';

                ++$i;
            }
        }

        $sDataGrid .= '</tbody>';

        $sDataGrid .= $this->_buildFooter ($iFirst, $iLast);

        $sDataGrid .= '</table>';

        $aReturn['table'] = $sDataGrid;
        $aReturn['results'] = $this->iRowCount;
        $aReturn['page'] = $this->iPageNum;
        $aReturn['order'] = $this->sOrder;
        $aReturn['filter'] = $this->sFilter;
        $aReturn['resultsperpage'] = $this->iResultsPerPage;

        SM()->output->setType ('application/json');

        echo json_encode ($aReturn);
    }

    /**
     * Prints out script to handle Ajax data grids
     *
     * @param string $sResponse Response script
     * @param string $sParams JavaScript object thats sent to the response script
     * @param string $sPrefix Prefix that matches the ajax response
     */
    public static function printAjaxCall ($sResponse = '', $sParams = '', $sPrefix = '')
    {
        // If no response script is set, use the current script
        if ( empty ($sResponse) )
        {
            $sResponse = $_SERVER['PHP_SELF'];
        }

        if ( empty ($sParams) or is_null ($sParams) )
        {
            $sParams = '{}';
        }
        ?>
        <script type="text/javascript">
            //<![CDATA[
            var dg<?=$sPrefix?>;

            if ( typeof DataGrid != 'function' )
            {
                function DataGrid (response, params, prefix) {
                    this.prefix = prefix;
                    this.page = 1;
                    this.order = '';
                    this.filter = '';
                    this.resultsperpage = '';
                    this.params = params;
                    this.action = '';
                    this.results = 0;
                    this.response = response;
                    this.updateTable ();
                }
            }
            DataGrid.prototype.$ = function (e) {
                return document.getElementById (e);
            };
            DataGrid.prototype.ajaxPost = function(u,f,a) {
                try {
                    var x = new ActiveXObject('Msxml2.XMLHTTP')
                }
                catch(e) {
                    try{
                        var x = new ActiveXObject('Microsoft.XMLHTTP')
                    }
                    catch(e){
                        var x = new XMLHttpRequest()
                    }
                }

                x.open('POST',u,true);
                x.onreadystatechange = function() {
                    if(x.readyState == 4)
                        f(x.responseText);
                };
                x.setRequestHeader('Content-type','application/x-www-form-urlencoded');
                x.setRequestHeader( 'X-Requested-With', 'XMLHttpRequest');
                x.send(a);
            };
            DataGrid.prototype.setOpacity = function(element, value) {
                element.style.opacity = (value == 1 || value === '') ? '' :
                    (value < 0.00001) ? 0 : value;
            };
            DataGrid.prototype.setPage = function (page) {
                this.page = page;
                this.action = 'page';
                this.updateTable ();
            };
            DataGrid.prototype.setOrder = function (column, order) {
                this.order = column + ':' + order;
                this.action = 'order';
                this.updateTable ();
            };
            DataGrid.prototype.setFilter = function (column) {
                this.page = 1;
                this.filter = column + ':' + this.$(this.prefix + 'filter-value-' + column).value;
                this.action = 'filter';
                this.updateTable ();
            };
            DataGrid.prototype.setFilterOnKeyPress = function (column) {
                this.page = 1;
                this.filter = column + ':' + this.$(this.prefix + 'filter-value-' + column).value;
                this.action = 'filter';
                if(event.keyCode == '13'){
                    this.updateTable ();
                }
            };
            DataGrid.prototype.clearFilter = function () {
                this.page = 1;
                this.filter = '';
                this.action = 'filter';
                this.updateTable ();
            };
            DataGrid.prototype.showHideFilter = function (column) {
                if ( this.$(this.prefix + 'filter-' + column).style.display != 'none' )
                    this.clearFilter ();
                else
                    this.$(this.prefix + 'filter-' + column).style.display = 'block';
            };
            DataGrid.prototype.reset = function () {
                this.page = 1;
                this.order = '';
                this.filter = '';
                this.action = 'all';
                this.updateTable ();
            };
            DataGrid.prototype.print = function () {
                window.print ();
            };
            DataGrid.prototype.getResults = function () {
                return this.results;
            };
            DataGrid.prototype.getPage = function () {
                return this.page;
            };
            DataGrid.prototype.setResultsPerPage = function (num) {
                this.action = 'setresults';
                this.page = 1;
                this.resultsperpage = num;
                this.updateTable ();
            };
            DataGrid.prototype.updateTable = function () {
                var el = this.$('datagrid' + this.prefix);

                this.setOpacity (el, 0.5);

                var args = 'page=' + escape (this.page) + '&order=' + escape (this.order) + '&filter=' + escape (this.filter) + '&resultsperpage=' + escape (this.resultsperpage) + '&prefix=' + escape (this.prefix) + '&action=' + escape (this.action) + '&response=' + escape (this.response);

                for (var name in this.params)
                {
                    args += '&' + name + '=' + escape (this.params[name]);
                }

                this.ajaxPost (this.response, function (transport) {
                    DataGrid.prototype.setOpacity (el, 1);

                    var j = eval('('+transport+')');

                    if ( transport && j )
                    {
                        this.page = j.page;
                        this.results = j.results;
                        this.order = j.order;
                        this.filter = j.filter;
                        this.resultsperpage = j.resultsperpage;
                        el.innerHTML = j.table;
                    }
                    else
                    {
                        el.innerHTML = "Failed to load table.";
                    }

                },args);

                this.action = '';
            };
            //]]>
        </script>
        <div id="datagrid<?=$sPrefix?>">
            <b>Loading...</b>
        </div>
        <script type="text/javascript">
            //<![CDATA[
            dg<?=$sPrefix?> = new DataGrid ('<?=$sResponse?>', <?=$sParams?>, '<?=$sPrefix?>');
            //]]>
        </script>

        <?php
    }
}