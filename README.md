# SimpleDataGrid
Used for creating dynamics HTML tables based on SQL query. Use is discouraged and is
only maintained for backwards compatibility with older TEAM Framework based applications.

The package uses functions and class that are part of TEAM Framework and cannot be used outside of it.

## Install with Composer
To install with [Composer](https://getcomposer.org/), simply require the latest version of this package.
```
composer require team/simpledatagrid:dev-master
```